requires 'perl', '5.20.3';
requires 'Mojolicious', '== 6.48';
requires 'Mojo::Pg', '== 2.23';
requires 'IO::Socket::SSL', '== 2.024';
requires 'Readonly', '== 2.00';
