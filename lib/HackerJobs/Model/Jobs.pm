package HackerJobs::Model::Jobs;
use Mojo::Base -base;

use 5.020;
use experimental 'signatures';

our $VERSION = '1.002';
$VERSION = eval $VERSION;    ## no critic (ConstantVersion, StringyEval)

has 'pg';

sub add($self, $job)
{
  my $sql
    = 'INSERT INTO jobs (id, by, time, body) VALUES (?, ?, to_timestamp(?), ?)';

  $self->pg->db->query($sql, $job->{id}, $job->{by}, $job->{time},
    $job->{body});

  return 1;
}

sub get_page($self, $page = 1, $limit = 25)
{
  my $offset = ($page - 1) * $limit;
  my $sql    = qq{
    SELECT id, by, time::date AS time, body
    FROM jobs
    ORDER BY id DESC
    LIMIT ?
    OFFSET ?
  };

  return $self->pg->db->query($sql, $limit, $offset)->hashes->to_array;
}

sub get_last_saved_id($self)
{
  my $sql = 'SELECT coalesce(max(id), 0) AS id FROM jobs';

  return $self->pg->db->query($sql)->hash->{id};
}

1;
