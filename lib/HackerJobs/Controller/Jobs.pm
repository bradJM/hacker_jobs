package HackerJobs::Controller::Jobs;
use Mojo::Base 'Mojolicious::Controller';

use 5.020;
use experimental 'signatures';

our $VERSION = '1.002';
$VERSION = eval $VERSION;    ## no critic (ConstantVersion, StringyEval)

sub home($self)
{
  my $page = $self->param('page') // 1;

  if (!$self->_validate_page($page)) {
    return $self->render(text => 'Bad request', status => 400);
  }

  my %response_data = (
    previous_page => $page - 1,
    next_page     => $page + 1,
    jobs          => $self->jobs->get_page($page),
  );

  return $self->respond_to(
    json => {json     => \%response_data},
    html => {template => 'jobs/home', %response_data}
  );
}

sub _validate_page($self, $page)
{
  my $validation = $self->validation;

  $validation->input({page => $page});
  $validation->required('page')->like(qr/\A[1-9]\d*\z/);

  return !$validation->has_error;
}

1;
