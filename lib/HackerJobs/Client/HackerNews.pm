package HackerJobs::Client::HackerNews;
use Mojo::Base -base;

use 5.020;
use experimental 'signatures';

use Mojo::UserAgent;
use Readonly;

our $VERSION = '1.002';
$VERSION = eval $VERSION;    ## no critic (ConstantVersion, StringyEval)

has 'base_url';
has 'user_agent';
has 'logger';

Readonly::Scalar my $WHO_IS_HIRING_USER_ID => 'whoishiring';

sub get_jobs($self, $last_saved_id = 0, $number_of_stories = 1)
{
  my @jobs;

  # find the whoishiring user's stories
  my $user = $self->_get_user($WHO_IS_HIRING_USER_ID);
  my $story_ids = [reverse sort { $a <=> $b } @{$user->{submitted}}];
  my @stories = $self->_get_live_hiring_stories($story_ids, $number_of_stories);

  # add each top-level reply to our dataset
  for my $story (@stories) {
    push @jobs, $self->_get_unsaved_replies($story, $last_saved_id);
  }

  return @jobs;
}

sub _get_item($self, $id)
{
  $self->logger->info("Fetching item $id.");
  my $item_url = $self->base_url->clone->path("item/$id.json");

  return $self->user_agent->get($item_url)->res->json;
}

sub _get_user($self, $id)
{
  $self->logger->info("Fetching user $id.");
  my $user_url = $self->base_url->clone->path("user/$id.json");

  return $self->user_agent->get($user_url)->res->json;
}

sub _is_live($self, $item)
{
  return !(exists $item->{dead} || $item->{deleted});
}

sub _is_story($self, $item)
{
  return $item->{type} eq 'story';
}

sub _is_comment($self, $item)
{
  return $item->{type} eq 'comment';
}

sub _is_live_story($self, $item)
{
  return $self->_is_live($item) && $self->_is_story($item);
}

sub _is_live_comment($self, $item)
{
  return $self->_is_live($item) && $self->_is_comment($item);
}

sub _is_live_hiring_story($self, $item)
{
  return $self->_is_live_story($item)
    && $item->{title} =~ /\AAsk HN: Who is hiring[?]/;
}

sub _get_live_hiring_stories($self, $ids, $number_of_stories = 1)
{
  my @hiring_stories;

  for my $id (@{$ids}) {
    my $item = $self->_get_item($id);

    if ($self->_is_live_hiring_story($item)) {
      $self->logger->info("Adding $item->{title} to hiring stories.");
      push @hiring_stories, $item;
    }

    last if @hiring_stories >= $number_of_stories;
  }

  return @hiring_stories;
}

sub _get_unsaved_replies($self, $story, $last_saved_id)
{
  my @replies;
  my @kids = reverse sort { $a <=> $b } @{$story->{kids}};

  for my $id (@kids) {
    last if $id <= $last_saved_id;

    my $item = $self->_get_item($id);

    if ($self->_is_live_comment($item)) {
      $self->logger->info("Adding $item->{id} to replies.");
      my $reply = {
        id   => $item->{id},
        by   => $item->{by},
        time => $item->{time},
        body => $item->{text},
      };

      push @replies, $reply;
    }
  }

  return @replies;
}

1;
