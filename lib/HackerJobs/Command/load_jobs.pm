## no critic (Capitalization)
package HackerJobs::Command::load_jobs;
## use critic
use Mojo::Base 'Mojolicious::Command';

use 5.020;
use experimental 'signatures';

our $VERSION = '1.002';
$VERSION = eval $VERSION;    ## no critic (ConstantVersion, StringyEval)

has description => 'Load Hacker News job post comments into the database';
has usage => sub { shift->extract_usage };

sub run($self, $number_of_stories = 1)
{
  $self->app->logger->info(
    "Fetching jobs from $number_of_stories story / stories.");

  my $last_saved_id = $self->app->jobs->get_last_saved_id();

  $self->app->logger->info("Last saved ID is $last_saved_id.");

  my @jobs = $self->app->client->get_jobs($last_saved_id, $number_of_stories);

  for my $job (@jobs) {
    $self->app->jobs->add($job);
  }

  return;
}

1;
