package HackerJobs;
use Mojo::Base 'Mojolicious';

use 5.020;
use experimental 'signatures';

our $VERSION = '1.002';
$VERSION = eval $VERSION;    ## no critic (ConstantVersion, StringyEval)

use HackerJobs::Client::HackerNews;
use HackerJobs::Model::Jobs;
use Mojo::Log;
use Mojo::Pg;
use Mojo::URL;
use Mojo::UserAgent;

# This method will run once at server start
sub startup ($self)
{
  # Config
  $self->plugin('Config');
  $self->secrets($self->config('secrets'));

  # Commands
  push @{$self->commands->namespaces}, 'HackerJobs::Command';

  # Logger
  $self->helper(
    logger => sub ($self) {
      state $logger = Mojo::Log->new();
    }
  );

  # Client
  $self->helper(
    client => sub ($self) {
      state $client = HackerJobs::Client::HackerNews->new(
        base_url   => Mojo::URL->new($self->config('hacker_news_api_url')),
        user_agent => Mojo::UserAgent->new(),
        logger     => $self->logger
      );
    }
  );

  # Model
  $self->helper(pg =>
      sub ($self) { state $pg = Mojo::Pg->new($self->config('database_url')) });

  $self->helper(
    jobs => sub ($self) {
      state $jobs = HackerJobs::Model::Jobs->new(pg => $self->pg);
    }
  );

  # Migrate
  my $migrations_path
    = $self->home->rel_file('resources/migrations/hacker_jobs.sql');
  $self->pg->migrations->from_file($migrations_path)->migrate();

  # Routes
  my $r = $self->routes;

  $r->get(q{/})->to('jobs#home')->name('jobs_home');

  return;
}

1;
