-- 1 up
CREATE TABLE IF NOT EXISTS jobs (
    id    INTEGER     PRIMARY KEY,
    by    TEXT        NOT NULL,
    time  TIMESTAMPTZ NOT NULL,
    body  TEXT        NOT NULL
);

-- 1 down
DROP TABLE IF EXISTS jobs;
