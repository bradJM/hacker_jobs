use Mojo::Base -strict;

use Test::More;
use Test::Mojo;
use Readonly;

Readonly::Scalar my $STATUS_OK          => 200;
Readonly::Scalar my $STATUS_BAD_REQUEST => 400;

my $t = Test::Mojo->new('HackerJobs');
$t->get_ok('/')->status_is($STATUS_OK)->content_like(qr/Hacker Jobs/i);

# page validation
$t->get_ok('/')->status_is($STATUS_OK);
$t->get_ok('/?page=1')->status_is($STATUS_OK);
$t->get_ok('/?page=9')->status_is($STATUS_OK);
$t->get_ok('/?page=10')->status_is($STATUS_OK);

$t->get_ok('/?page=0')->status_is($STATUS_BAD_REQUEST);
$t->get_ok('/?page=-1')->status_is($STATUS_BAD_REQUEST);
$t->get_ok('/?page=010')->status_is($STATUS_BAD_REQUEST);
$t->get_ok('/?page=abc')->status_is($STATUS_BAD_REQUEST);
$t->get_ok('/?page=1a')->status_is($STATUS_BAD_REQUEST);

# content negotiation
$t->get_ok('/', {Accept => 'text/html'})->content_type_like(qr/text\/html/);
$t->get_ok('/', {Accept => 'application/json'})->content_type_like(qr/application\/json/);

done_testing();
